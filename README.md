This is a light version of Characters fight game available from "https://gitlab.com/sajjadgameactor/CharacterFight" 
In this version by removing GUI the game can be simulated Several thousand times per minute by AI.
we use this simulation result to Check out the balance of the game by calculating win rate of each card. but you can test your own balancing algorithm with this game or  use it for any other acadmic research.

the project contain a file named "Cardinfo", all cards attribute is defined in this file and you can change that value of each attribute in that file.
current version of game contain of 50 cards based on famous characters but we are currently working on a new version of the game with more than hundred card Along with more sophisticated mechanics like Hearthstone so the balancing result will be more reliable.

![Main Panel](ReadmeFiles/panel.JPG)