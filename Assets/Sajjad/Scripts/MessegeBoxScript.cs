﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessegeBoxScript : MonoBehaviour {

    [SerializeField]
    Image rayBlockerImage;

    [SerializeField]
    Text header, content;

    public void ShowMessegeBox(string contentText, string headerText = "",bool blockRays = false)
    {
        header.text = headerText;
        content.text = contentText;

        rayBlockerImage.raycastTarget = blockRays;
        gameObject.SetActive(true);
    }

    public void OnOkButtonClick()
    {
        gameObject.SetActive(false);
    }
}
