﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum InteractionAffected
{
    None,
    Self,
    LeaderEnemy,
    LeaderTeammate,
    DefinedEnemy,
    DefinedTeammate,
    RandomEnemyGroundCard,
    RandomTeammateGroundCard,
    RandomEnemyGround,    // all card and leader
    RandomTeammateGround, // all card and leader
    RandomEnemyHand,
    RandomTeammateHand,
    AllEnemyCard,
    AllTeammateCard,
    AllEnemyLowerHealthThanVariable,
    LowestManaEnemy,
    AllDamaged,
}

public class InteractionEvent
{
    public InteractionType interactionType;
    public InteractionAffected interactionAffected;
    public Turn subjectPlayer;
    public int subjectID;
    public Turn objectPlayer;
    public int objectID;
    public int power;

    public int variable1; // Auxiliary
    public int variable2; // Auxiliary

    public InteractionEvent() { }

    public InteractionEvent(Turn applicantPlayer,InteractionType type,InteractionAffected affected, int subID = -1, int pow = -1, int vicD = -1)
    {
        subjectPlayer = applicantPlayer;
        interactionType = type;
        interactionAffected = affected;
        power = pow;
        subjectID = subID;
        objectID = vicD;
    }

}
