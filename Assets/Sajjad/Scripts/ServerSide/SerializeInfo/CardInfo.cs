﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine.Serialization;
using System;
using LitJson;

public enum CardCategory
{
    Girl, Boy, Common, Leader
}


public enum CardsName
{
    None = 0,
    mikimouse = 1,
    jerry = 2,
    tom = 3,
    Spike = 4,
    Baymax = 5,
    Mike = 6,
    Genie = 7,
    lionKing = 8,
    Sulley = 9,
    Olaf = 10,
    Judy = 11,
    wall_E = 12,
    nemo = 13,
    Bruce = 14,
    kingkong = 15,
    metekoman = 16,
    shrek = 17,
    bibbib = 18,
    gorg = 19,
    kotoleh7 = 20,

    // man
    luckyluke = 21,
    Tarzan = 22,
    Hercules = 23,
    Aladdin = 24,
    hulk = 25,
    ironMan = 26,
    superMan = 27,
    gru = 28,
    spiderMan = 29,
    benten = 30,
    JackSparrow = 31,

    // girls
    snowWhite = 32,
    cindrella = 33,
    Rapunzel = 34,
    mrida = 35,
    anna = 36,
    mulan = 37,
    agnes = 38,
    JudyAbbott = 39,

    minion1 = 40,
    minion2 = 41,
    minionMuscular = 42,
    minionWolv = 43,
    WildmuttBen = 44,
    mushu = 45,
    leng = 46,
    po = 47,
    tigress = 48,
    Shifu = 49,
    Oogway = 50,
    mantis = 51,
    viper = 52,
    Tai_Lung = 53,
    Commander_vachir = 54,
    Kai = 55,


    monky = 101,



    // Leaders
    LeaderElsa = 1001,
    LeaderBatman = 1002
}



[Serializable]
public class CardInfo
{

    public CardCategory category;
    public int cardID;
    public CardsName name;
    public int mana;
    public int power;
    public int extraPower;
    public int eachTurnAttackNumber = 1;
    public int currentTurnAttackNumber = 0;
    public bool rushAbility = false;
    public bool isHidden = false;
    public bool isTerminator = false;
    public bool isDead = false;
    [SerializeField]
    public int maxHealth { get; private set; }
    [SerializeField]
    public int currentHealth { get; private set; }

    public double criticalChance = 0;
    public double criticalX = 1; // float have serialize problem

    //public float luckChance = 0;
    //public float luckX = 1;

    public bool isDefender = false;
    public List<CardsName> chainedCards = new List<CardsName>(); // the cards that should add to hand when this card enter to ground
    public List<CardsName> chainedCardsOnExit = new List<CardsName>(); // the cards that should add to hand when this card enter to ground

    public Turn myPlayer;

    #region Menu Value

    public int maxSelectable = 3; // how many of this card can be selected to deck in menu

    #endregion

    [SerializeField]
    int stunTurn = -1;
    [SerializeField]
    int attackMana = 0;

    public List<InteractionEvent> OnEnterGroundInteractions = new List<InteractionEvent>();
    public List<InteractionEvent> OnStayGroundInteractions = new List<InteractionEvent>();
    public List<InteractionEvent> OnEachTurnInteractions = new List<InteractionEvent>();
    public List<InteractionEvent> OnExitGroundInteractions = new List<InteractionEvent>();
    public CardsName deadTransform = CardsName.None;


    public CardInfo() // need for litjson
    { }

    public CardInfo(CardsName cardName, int id, Turn Player)
    {
        cardID = id;
        myPlayer = Player;
        InitCard(cardName);
    }

    public void TransformCard(CardsName name)
    {
        InitCard(name);
        deadTransform = CardsName.None;
        isDead = false;
    }


    void SetMainAttribute(CardsName cardName, CardCategory cat, int cardHelth, int cardPower, int cardMana)
    {
        name = cardName;
        category = cat;
        maxHealth = currentHealth = cardHelth;
        mana = cardMana;
        power = cardPower;


    }

    public void SetHealth(int health)
    {
        maxHealth = currentHealth = health;

    }

    public void Heal(int value)
    {
        currentHealth = Mathf.Min(currentHealth + value, maxHealth);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="power"></param>
    /// <returns>if dead return true else return false</returns>
    public bool Impact(int power)
    {
        currentHealth -= power;

        if (currentHealth <= 0)
            return true;
        else
            return false;
    }

    public bool IsDamaged()
    {
        if (currentHealth != maxHealth)
            return true;
        else
            return false;
    }

    void AddRushAbility()
    {
        rushAbility = true;
        currentTurnAttackNumber = eachTurnAttackNumber;
    }

    public void Stun(int turn)
    {
        stunTurn = turn;
    }

    public bool CanAttack()
    {
        if (currentTurnAttackNumber > 0 && stunTurn == -1)
            return true;

        return false;
    }

    public InteractionInfo Go2NextTurn()
    {
        currentTurnAttackNumber = eachTurnAttackNumber;
        InteractionInfo info = null;
        if (stunTurn >= 0)
        {
            stunTurn--;

            if (stunTurn == -1)
            {
                info = new InteractionInfo();
                info.InitRemoveEffect(myPlayer, cardID);
            }

        }

        return info;
    }

    public int GetCurrentPower()
    {
        return power + extraPower;
    }

    void InitCard(CardsName name)
    {
        InteractionEvent tempInteraction;
        switch (name)
        {
            case CardsName.None:
                SetMainAttribute(name, CardCategory.Common, 0, 0, 0);
                break;
            case CardsName.mikimouse:
                SetMainAttribute(name, CardCategory.Common, 3, 2, 2);
                break;
            case CardsName.tom:
                SetMainAttribute(name, CardCategory.Common, 1, 2, 1);
                break;
            case CardsName.jerry:
                SetMainAttribute(name, CardCategory.Common, 2, 1, 1);
                break;
            case CardsName.Spike:
                SetMainAttribute(name, CardCategory.Common, 3, 3, 3);
                break;
            case CardsName.Baymax:
                SetMainAttribute(name, CardCategory.Common, 7, 1, 4);
                isDefender = true;
                break;
            case CardsName.Mike:
                SetMainAttribute(name, CardCategory.Common, 1, 2, 2);
                AddRushAbility();
                break;
            case CardsName.Genie:
                SetMainAttribute(name, CardCategory.Common, 3, 5, 4);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.NewCard, InteractionAffected.None, cardID, 2);
                tempInteraction.variable1 = 0; // random
                OnEnterGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.lionKing:
                SetMainAttribute(name, CardCategory.Common, 7, 6, 6);
                break;
            case CardsName.Sulley:
                SetMainAttribute(name, CardCategory.Common, 7, 3, 4);
                break;
            case CardsName.Olaf:
                SetMainAttribute(name, CardCategory.Common, 1, 1, 1);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.Frozen, InteractionAffected.RandomEnemyGroundCard, cardID, 1);
                OnEnterGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.Judy:
                SetMainAttribute(name, CardCategory.Common, 1, 4, 3);
                break;
            case CardsName.wall_E:
                SetMainAttribute(name, CardCategory.Common, 3, 1, 3);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.Heal, InteractionAffected.LeaderTeammate, cardID, 3);
                OnExitGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.nemo:
                SetMainAttribute(name, CardCategory.Common, 1, 2, 2);
                isHidden = true;
                break;
            case CardsName.Bruce:
                SetMainAttribute(name, CardCategory.Common, 4, 5, 4);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.RemoveHiddenSpecialAbility, InteractionAffected.AllEnemyCard, cardID);
                OnEnterGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.kingkong:
                SetMainAttribute(name, CardCategory.Common, 6, 8, 8);
                break;
            case CardsName.metekoman:
                SetMainAttribute(name, CardCategory.Common, 2, 4, 4);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.ExtraPower, InteractionAffected.AllTeammateCard, cardID, 1);
                OnStayGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.shrek:
                SetMainAttribute(name, CardCategory.Common, 4, 2, 3);
                break;
            case CardsName.bibbib:
                SetMainAttribute(name, CardCategory.Common, 1, 3, 3);
                AddRushAbility();
                break;
            case CardsName.gorg:
                SetMainAttribute(name, CardCategory.Common, 6, 2, 4);
                isDefender = true;
                break;
            case CardsName.kotoleh7:
                SetMainAttribute(name, CardCategory.Common, 7, 2, 5);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.ExtraPower, InteractionAffected.Self, cardID, 1);
                OnEachTurnInteractions.Add(tempInteraction);
                break;
            case CardsName.minion1:
                SetMainAttribute(name, CardCategory.Common, 1, 1, 1);
                isDefender = true;
                break;
            case CardsName.minion2:
                SetMainAttribute(name, CardCategory.Common, 1, 1, 1);
                isDefender = true;
                break;
            case CardsName.WildmuttBen:
                SetMainAttribute(name, CardCategory.Common, 5, 4, 4);
                break;
            case CardsName.mushu:
                SetMainAttribute(name, CardCategory.Common, 3, 1, 2);
                break;
            case CardsName.monky:
                SetMainAttribute(name, CardCategory.Common, 1, 0, 0);
                break;
            case CardsName.leng:
                SetMainAttribute(name, CardCategory.Common, 4, 3, 4);
                break;
            case CardsName.minionMuscular:
                SetMainAttribute(name, CardCategory.Common, 2, 2, 2);
                isDefender = true;
                break;
            case CardsName.minionWolv:
                SetMainAttribute(name, CardCategory.Common, 1, 1, 1);
                AddRushAbility();
                break;

            case CardsName.snowWhite:
                SetMainAttribute(name, CardCategory.Common, 5, 1, 6);
                chainedCards.Add(CardsName.kotoleh7);
                eachTurnAttackNumber = 2;
                break;
            case CardsName.cindrella:
                SetMainAttribute(name, CardCategory.Common, 3, 3, 5);
                isDefender = true;
                break;
            case CardsName.Rapunzel:
                SetMainAttribute(name, CardCategory.Common, 4, 5, 7);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.Heal, InteractionAffected.Self, cardID, 1);
                OnEachTurnInteractions.Add(tempInteraction);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.Heal, InteractionAffected.AllTeammateCard, cardID, 1);
                OnEachTurnInteractions.Add(tempInteraction);
                break;
            case CardsName.mrida:
                SetMainAttribute(name, CardCategory.Common, 4, 8, 7);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.MagicalImpact, InteractionAffected.LeaderEnemy, cardID, 3);
                OnEnterGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.anna:
                SetMainAttribute(name, CardCategory.Common, 5, 3, 4);
                AddRushAbility();
                break;
            case CardsName.mulan:
                SetMainAttribute(name, CardCategory.Common, 3, 3, 5);
                deadTransform = CardsName.mushu;
                break;
            case CardsName.agnes:
                SetMainAttribute(name, CardCategory.Common, 1, 1, 1);
                eachTurnAttackNumber = 2;
                break;
            case CardsName.JudyAbbott:
                SetMainAttribute(name, CardCategory.Common, 1, 4, 6);
                deadTransform = CardsName.leng;
                break;
            case CardsName.luckyluke:
                SetMainAttribute(name, CardCategory.Common, 2, 2, 3);
                criticalChance = 0.5f;
                criticalX = 2;
                break;
            case CardsName.Tarzan:
                SetMainAttribute(name, CardCategory.Common, 4, 2, 3);
                chainedCards.Add(CardsName.kingkong);
                break;
            case CardsName.Hercules:
                SetMainAttribute(name, CardCategory.Common, 4, 5, 4);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.MagicalImpact, InteractionAffected.LeaderEnemy, cardID, 1);
                OnEnterGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.Aladdin:
                SetMainAttribute(name, CardCategory.Common, 2, 2, 3);
                isHidden = true;
                break;
            case CardsName.hulk:
                SetMainAttribute(name, CardCategory.Common, 8, 9, 9);
                break;
            case CardsName.ironMan:
                SetMainAttribute(name, CardCategory.Common, 5, 7, 7);
                isTerminator = true;
                break;
            case CardsName.superMan:
                SetMainAttribute(name, CardCategory.Common, 6, 7, 6);
                break;
            case CardsName.gru:
                SetMainAttribute(name, CardCategory.Common, 4, 5, 5);
                deadTransform = CardsName.minion1;
                break;
            case CardsName.spiderMan:
                SetMainAttribute(name, CardCategory.Common, 7, 4, 7);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.SpiderWeb, InteractionAffected.AllDamaged, cardID, 1);
                OnEnterGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.benten:
                SetMainAttribute(name, CardCategory.Common, 2, 5, 8);
                deadTransform = CardsName.WildmuttBen;
                break;
            case CardsName.JackSparrow:
                SetMainAttribute(name, CardCategory.Common, 5, 3, 5);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.Transform, InteractionAffected.LowestManaEnemy, cardID);
                tempInteraction.power = (int)CardsName.monky;
                OnExitGroundInteractions.Add(tempInteraction);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.NewCard, InteractionAffected.None, cardID, 1);
                tempInteraction.variable1 = 1;
                tempInteraction.variable2 = 6;
                OnEnterGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.po:
                SetMainAttribute(name, CardCategory.Common, 9, 3, 8);
                criticalChance = 0.33f;
                criticalX = 3;
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.Heal, InteractionAffected.Self, cardID, 3);
                OnEachTurnInteractions.Add(tempInteraction);
                break;
            case CardsName.tigress:
                SetMainAttribute(name, CardCategory.Common, 5, 4, 4);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.MagicalImpact, InteractionAffected.LeaderEnemy, cardID, 2);
                OnEnterGroundInteractions.Add(tempInteraction);
                break;
            case CardsName.Shifu:
                SetMainAttribute(name, CardCategory.Common, 3, 5, 5);
                AddRushAbility();
                break;
            case CardsName.Oogway:
                SetMainAttribute(name, CardCategory.Common, 8, 4, 7);
                isHidden = true;
                break;
            case CardsName.mantis:
                SetMainAttribute(name, CardCategory.Common, 2, 5, 4);
                isHidden = true;
                break;
            case CardsName.viper:
                SetMainAttribute(name, CardCategory.Common, 2, 4, 3);
                isTerminator = true;
                break;
            case CardsName.Tai_Lung:
                SetMainAttribute(name, CardCategory.Common, 6, 7, 8);
                tempInteraction = new InteractionEvent(myPlayer, InteractionType.ExtraPower, InteractionAffected.Self, cardID, 2);
                OnEachTurnInteractions.Add(tempInteraction);
                break;
            case CardsName.Commander_vachir:
                SetMainAttribute(name, CardCategory.Common, 3, 8, 6);
                isDefender = true;
                break;
                case CardsName.Kai:
                SetMainAttribute(name, CardCategory.Common, 7, 5, 7);
                criticalChance = 0.5;
                criticalX = 2;
                break;


            case CardsName.LeaderElsa:
                SetMainAttribute(name, CardCategory.Leader, 30, 0, -1);
                currentTurnAttackNumber = eachTurnAttackNumber = 0;
                break;
            case CardsName.LeaderBatman:
                SetMainAttribute(name, CardCategory.Leader, 30, 0, -1);
                currentTurnAttackNumber = eachTurnAttackNumber = 0;
                break;
            default:
                break;
        }

        string nameString = name.ToString();

        if(name != CardsName.LeaderElsa && name != CardsName.LeaderBatman)
        {
            float balanceFactor = 1;
            foreach (var item in MassiveLogicTester.balancedItemList)
            {

                if (string.Equals(nameString, item.items.name))
                {
                    balanceFactor = float.Parse(item.items.balance_factor);
                    break;
                }
            }

            try
            {
                int newMana = Mathf.Clamp(Mathf.RoundToInt(mana / balanceFactor), 0, 9);
                mana = newMana;

            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
            //int newMana = Mathf.RoundToInt(mana / balanceFactor);
            //mana = newMana;
        }


        //switch (name) // expert
        //{
        //    case CardsName.None:
        //        SetMainAttribute(name, CardCategory.Common, 0, 0, 0);
        //        break;
        //    case CardsName.mikimouse:
        //        SetMainAttribute(name, CardCategory.Common, 3, 2, 2);
        //        break;
        //    case CardsName.jerry:
        //        SetMainAttribute(name, CardCategory.Common, 2, 1, 1);
        //        break;
        //    case CardsName.tom:
        //        SetMainAttribute(name, CardCategory.Common, 1, 2, 1);
        //        break;
        //    case CardsName.Spike:
        //        SetMainAttribute(name, CardCategory.Common, 3, 3, 3);
        //        break;
        //    case CardsName.Baymax:
        //        SetMainAttribute(name, CardCategory.Common, 7, 1, 6);
        //        isDefender = true;
        //        break;
        //    case CardsName.Mike:
        //        SetMainAttribute(name, CardCategory.Common, 1, 2, 2);
        //        AddRushAbility();
        //        break;
        //    case CardsName.Genie:
        //        SetMainAttribute(name, CardCategory.Common, 3, 5, 4);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.NewCard, InteractionAffected.None, cardID, 2);
        //        tempInteraction.variable1 = 0; // random
        //        OnEnterGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.lionKing:
        //        SetMainAttribute(name, CardCategory.Common, 6, 7, 6);
        //        break;
        //    case CardsName.Sulley:
        //        SetMainAttribute(name, CardCategory.Common, 7, 3, 5);
        //        break;
        //    case CardsName.Olaf:
        //        SetMainAttribute(name, CardCategory.Common, 1, 1, 1);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.Frozen, InteractionAffected.RandomEnemyGroundCard, cardID, 1);
        //        OnEnterGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.Judy:
        //        SetMainAttribute(name, CardCategory.Common, 1, 4, 3);
        //        break;
        //    case CardsName.wall_E:
        //        SetMainAttribute(name, CardCategory.Common, 3, 1, 3);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.Heal, InteractionAffected.LeaderTeammate, cardID, 3);
        //        OnExitGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.nemo:
        //        SetMainAttribute(name, CardCategory.Common, 1, 2, 2);
        //        isHidden = true;
        //        break;
        //    case CardsName.Bruce:
        //        SetMainAttribute(name, CardCategory.Common, 4, 5, 4);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.RemoveHiddenSpecialAbility, InteractionAffected.AllEnemyCard, cardID);
        //        OnEnterGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.kingkong:
        //        SetMainAttribute(name, CardCategory.Common, 6, 8, 7);
        //        break;
        //    case CardsName.metekoman:
        //        SetMainAttribute(name, CardCategory.Common, 2, 3, 5);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.ExtraPower, InteractionAffected.AllTeammateCard, cardID, 1);
        //        OnStayGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.shrek:
        //        SetMainAttribute(name, CardCategory.Common, 4, 2, 3);
        //        break;
        //    case CardsName.bibbib:
        //        SetMainAttribute(name, CardCategory.Common, 1, 3, 3);
        //        AddRushAbility();
        //        break;
        //    case CardsName.gorg:
        //        SetMainAttribute(name, CardCategory.Common, 5, 2, 4);
        //        isDefender = true;
        //        break;
        //    case CardsName.kotoleh7:
        //        SetMainAttribute(name, CardCategory.Common, 7, 3, 5);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.ExtraPower, InteractionAffected.Self, cardID, 1);
        //        OnEachTurnInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.minion1:
        //        SetMainAttribute(name, CardCategory.Common, 1, 0, 1);
        //        isDefender = true;
        //        break;
        //    case CardsName.minion2:
        //        SetMainAttribute(name, CardCategory.Common, 1, 0, 1);
        //        isDefender = true;
        //        break;
        //    case CardsName.WildmuttBen:
        //        SetMainAttribute(name, CardCategory.Common, 5, 4, 4);
        //        break;
        //    case CardsName.mushu:
        //        SetMainAttribute(name, CardCategory.Common, 3, 1, 2);
        //        break;
        //    case CardsName.monky:
        //        SetMainAttribute(name, CardCategory.Common, 1, 0, 0);
        //        break;
        //    case CardsName.leng:
        //        SetMainAttribute(name, CardCategory.Common, 5, 3, 4);
        //        break;
        //    case CardsName.minionMuscular:
        //        SetMainAttribute(name, CardCategory.Common, 2, 2, 2);
        //        isDefender = true;
        //        break;
        //    case CardsName.minionWolv:
        //        SetMainAttribute(name, CardCategory.Common, 1, 1, 1);
        //        AddRushAbility();
        //        break;

        //    case CardsName.snowWhite:
        //        SetMainAttribute(name, CardCategory.Common, 5, 2, 5);
        //        chainedCards.Add(CardsName.kotoleh7);
        //        eachTurnAttackNumber = 2;
        //        break;
        //    case CardsName.cindrella:
        //        SetMainAttribute(name, CardCategory.Common, 3, 3, 4);
        //        isDefender = true;
        //        break;
        //    case CardsName.Rapunzel:
        //        SetMainAttribute(name, CardCategory.Common, 5, 5, 7);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.Heal, InteractionAffected.Self, cardID, 1);
        //        OnEachTurnInteractions.Add(tempInteraction);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.Heal, InteractionAffected.AllTeammateCard, cardID, 1);
        //        OnEachTurnInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.mrida:
        //        SetMainAttribute(name, CardCategory.Common, 4, 8, 7);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.MagicalImpact, InteractionAffected.LeaderEnemy, cardID, 3);
        //        OnEnterGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.anna:
        //        SetMainAttribute(name, CardCategory.Common, 5, 3, 4);
        //        AddRushAbility();
        //        break;
        //    case CardsName.mulan:
        //        SetMainAttribute(name, CardCategory.Common, 3, 3, 5);
        //        deadTransform = CardsName.mushu;
        //        break;
        //    case CardsName.agnes:
        //        SetMainAttribute(name, CardCategory.Common, 1, 1, 1);
        //        eachTurnAttackNumber = 2;
        //        break;
        //    case CardsName.JudyAbbott:
        //        SetMainAttribute(name, CardCategory.Common, 1, 4, 6);
        //        deadTransform = CardsName.leng;
        //        break;
        //    case CardsName.luckyluke:
        //        SetMainAttribute(name, CardCategory.Common, 2, 2, 3);
        //        criticalChance = 0.5f;
        //        criticalX = 2;
        //        break;
        //    case CardsName.Tarzan:
        //        SetMainAttribute(name, CardCategory.Common, 4, 2, 3);
        //        chainedCards.Add(CardsName.kingkong);
        //        break;
        //    case CardsName.Hercules:
        //        SetMainAttribute(name, CardCategory.Common, 4, 4, 4);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.MagicalImpact, InteractionAffected.LeaderEnemy, cardID, 1);
        //        OnEnterGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.Aladdin:
        //        SetMainAttribute(name, CardCategory.Common, 2, 3, 3);
        //        isHidden = true;
        //        break;
        //    case CardsName.hulk:
        //        SetMainAttribute(name, CardCategory.Common, 8, 9, 9);
        //        break;
        //    case CardsName.ironMan:
        //        SetMainAttribute(name, CardCategory.Common, 5, 7, 6);
        //        isTerminator = true;
        //        break;
        //    case CardsName.superMan:
        //        SetMainAttribute(name, CardCategory.Common, 6, 7, 6);
        //        break;
        //    case CardsName.gru:
        //        SetMainAttribute(name, CardCategory.Common, 4, 5, 5);
        //        deadTransform = CardsName.minion1;
        //        break;
        //    case CardsName.spiderMan:
        //        SetMainAttribute(name, CardCategory.Common, 5, 6, 6);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.SpiderWeb, InteractionAffected.AllDamaged, cardID, 1);
        //        OnEnterGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.benten:
        //        SetMainAttribute(name, CardCategory.Common, 2, 5, 8);
        //        deadTransform = CardsName.WildmuttBen;
        //        break;
        //    case CardsName.JackSparrow:
        //        SetMainAttribute(name, CardCategory.Common, 5, 3, 5);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.Transform, InteractionAffected.LowestManaEnemy, cardID);
        //        tempInteraction.power = (int)CardsName.monky;
        //        OnExitGroundInteractions.Add(tempInteraction);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.NewCard, InteractionAffected.None, cardID, 1);
        //        tempInteraction.variable1 = 1;
        //        tempInteraction.variable2 = 6;
        //        OnEnterGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.po:
        //        SetMainAttribute(name, CardCategory.Common, 9, 3, 7);
        //        criticalChance = 0.33f;
        //        criticalX = 3;
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.Heal, InteractionAffected.Self, cardID, 3);
        //        OnEachTurnInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.tigress:
        //        SetMainAttribute(name, CardCategory.Common, 5, 4, 5);
        //        tempInteraction = new InteractionEvent(myPlayer, InteractionType.MagicalImpact, InteractionAffected.LeaderEnemy, cardID, 2);
        //        OnEnterGroundInteractions.Add(tempInteraction);
        //        break;
        //    case CardsName.Shifu:
        //        SetMainAttribute(name, CardCategory.Common, 3, 5, 6);
        //        AddRushAbility();
        //        break;
        //    case CardsName.Oogway:
        //        SetMainAttribute(name, CardCategory.Common, 9, 4, 6);
        //        isHidden = true;
        //        break;

        //    case CardsName.LeaderElsa:
        //        SetMainAttribute(name, CardCategory.Leader, 30, 0, -1);
        //        currentTurnAttackNumber = eachTurnAttackNumber = 0;
        //        break;
        //    case CardsName.LeaderBatman:
        //        SetMainAttribute(name, CardCategory.Leader, 30, 0, -1);
        //        currentTurnAttackNumber = eachTurnAttackNumber = 0;
        //        break;
        //    default:
        //        break;
        //}

    }

    public static string GetPersianName(CardsName cardName)
    {
        switch (cardName)
        {
            case CardsName.None:
                return "هیچی".faConvert();
            case CardsName.mikimouse:
                return "میکی موس".faConvert();
            case CardsName.jerry:
                return "جری".faConvert();
            case CardsName.tom:
                return "تام".faConvert();
            case CardsName.Spike:
                return "اسپایک".faConvert();
            case CardsName.Baymax:
                return "بایمکس".faConvert();
            case CardsName.Mike:
                return "مایک".faConvert();
            case CardsName.Genie:
                return "جنی".faConvert();
            case CardsName.lionKing:
                return "شیرشاه".faConvert();
            case CardsName.Sulley:
                return "سالی".faConvert();
            case CardsName.Olaf:
                return "الاف".faConvert();
            case CardsName.Judy:
                return "جودی".faConvert();
            case CardsName.wall_E:
                return "Wall-E".faConvert();
            case CardsName.nemo:
                return "نمو".faConvert();
            case CardsName.Bruce:
                return "بروس".faConvert();
            case CardsName.kingkong:
                return "کینگ کنگ".faConvert();
            case CardsName.metekoman:
                return "میتی کمان".faConvert();
            case CardsName.shrek:
                return "شرک".faConvert();
            case CardsName.bibbib:
                return "بیب بیب".faConvert();
            case CardsName.gorg:
                return "گرگی".faConvert();
            case CardsName.kotoleh7:
                return "7 کوتوله".faConvert();
            case CardsName.luckyluke:
                return "لوک خوش شانس".faConvert();
            case CardsName.Tarzan:
                return "تارزان".faConvert();
            case CardsName.Hercules:
                return "هرکولس".faConvert();
            case CardsName.Aladdin:
                return "علائدین".faConvert();
            case CardsName.hulk:
                return "هالک".faConvert();
            case CardsName.ironMan:
                return "مردآهنی".faConvert();
            case CardsName.superMan:
                return "سوپرمن".faConvert();
            case CardsName.gru:
                return "من نفرت انگیز".faConvert();
            case CardsName.spiderMan:
                return "مرد عنکبوتی".faConvert();
            case CardsName.benten:
                return "بن تن".faConvert();
            case CardsName.JackSparrow:
                return "جک گنجیشکه".faConvert();
            case CardsName.snowWhite:
                return "سفید برفی".faConvert();
            case CardsName.cindrella:
                return "سیندرلا".faConvert();
            case CardsName.Rapunzel:
                return "گیسوکمند".faConvert();
            case CardsName.mrida:
                return "مریدا".faConvert();
            case CardsName.anna:
                return "آنا".faConvert();
            case CardsName.mulan:
                return "مولان".faConvert();
            case CardsName.agnes:
                return "اگنیس".faConvert();
            case CardsName.JudyAbbott:
                return "جولی ابوت".faConvert();
            case CardsName.minion1:
                return "مینیون 1".faConvert();
            case CardsName.minion2:
                return "مینیون 2".faConvert();
            case CardsName.minionMuscular:
                return "مینیون عضله".faConvert();
            case CardsName.minionWolv:
                return "مینیون ولورین".faConvert();
            case CardsName.WildmuttBen:
                return "سگ فضایی".faConvert();
            case CardsName.mushu:
                return "موشو".faConvert();
            case CardsName.monky:
                return "میمون".faConvert();
            case CardsName.leng:
                return "بابا لنگ دراز".faConvert();
            case CardsName.LeaderElsa:
                return "السا".faConvert();
            case CardsName.LeaderBatman:
                return "بتمن".faConvert();
            default:
                return cardName.ToString();
        }

    }

    public string CardInfo2Json()
    {
        return JsonMapper.ToJson(this);
    }


    public static CardInfo Json2CardInfo(string json)
    {
        return JsonMapper.ToObject<CardInfo>(json);
    }

    public void RemoveStunEffect()
    {
        stunTurn = -1;
    }


    public string GetDiscription()
    {
        string description = "";

        if (isDefender)
            description += ("مدافع :تا زمانی که این کارت در زمین باشد به کارت های دیگر نمیتوان حمله کرد \n").faConvert();
        if (rushAbility)
            description += "یورش :از همان دور اول میتواند حمله کند \n".faConvert();

        if (criticalChance != 0)
            description += (" کریتیکال " + criticalX.ToString() + "X \n").faConvert();
        if (isHidden)
            description += "مخفی :تا زمانی که حمله نکند نمیتوان به آن حمله کرد \n".faConvert();
        if (isTerminator)
            description += "نابودگر :کارتی که آن را نابود کند نابود میشود \n".faConvert();
        if (deadTransform != CardsName.None)
            description += (" پس از نابودی به یک کارت  " + GetPersianName(deadTransform).ToString() + " تبدیل میشود \n").faConvert();



        switch (name)
        {
            case CardsName.None:
                break;
            case CardsName.mikimouse:
                break;
            case CardsName.jerry:
                break;
            case CardsName.tom:
                break;
            case CardsName.Spike:
                break;
            case CardsName.Baymax:
                break;
            case CardsName.Mike:
                break;
            case CardsName.Genie:
                description += "هنگام ورود 2 کارت کشیده میشود  \n".faConvert();
                break;
            case CardsName.lionKing:
                break;
            case CardsName.Sulley:
                break;
            case CardsName.Olaf:
                description += "هنگام ورود یک کارت حریف منجمد میشود  \n".faConvert();
                break;
            case CardsName.Judy:
                break;
            case CardsName.wall_E:
                description += "هنگام نابودی 3 سلامت به فرمانده میدهد  \n".faConvert();
                break;
            case CardsName.nemo:
                break;
            case CardsName.Bruce:
                description += "کارت ها از حالت مخفی خارج میشوند  \n".faConvert();
                break;
            case CardsName.kingkong:
                break;
            case CardsName.metekoman:
                description += "یک قدرت ضربه به همه کارت ها اضافه میشود  \n".faConvert();
                break;
            case CardsName.shrek:
                break;
            case CardsName.bibbib:
                break;
            case CardsName.gorg:
                break;
            case CardsName.kotoleh7:
                description += "پس از هر نوبت یک قدرت ضربه به کارت اضافه میشود  \n".faConvert();
                break;
            case CardsName.luckyluke:
                break;
            case CardsName.Tarzan:
                description += "یک کارت کینگ کنگ کشیده میشود  \n".faConvert();
                break;
            case CardsName.Hercules:
                description += "هنگام ورود 2 ضربه به فرمانده حریف میزند \n".faConvert();
                break;
            case CardsName.Aladdin:
                break;
            case CardsName.hulk:
                break;
            case CardsName.ironMan:
                break;
            case CardsName.superMan:
                break;
            case CardsName.gru:
                break;
            case CardsName.spiderMan:
                description += "هنگام ورود تمام کارت های زخمی استان میشوند  \n".faConvert();
                break;
            case CardsName.benten:
                break;
            case CardsName.JackSparrow:
                description += "پس از نابودی ضعیف ترین کارت حریف را به میمون تبدیل میکند   \n".faConvert();
                description += "یک کارت با انرژی بالا کشیده میشود  \n".faConvert();
                break;
            case CardsName.snowWhite:
                description += "هنگام ورود یک کارت هفت کوتوله کشیده میشود \n".faConvert();
                break;
            case CardsName.cindrella:
                break;
            case CardsName.Rapunzel:
                description += "در ابتدای هر نوبت 2 سلامتی  خود و یک سلامتی  تمام کارت ها را ترمیم میدهد \n".faConvert();
                break;
            case CardsName.mrida:
                description += "هنگام ورود 2 ضربه به فرمانده حریف میزند \n".faConvert();
                break;
            case CardsName.anna:
                break;
            case CardsName.mulan:
                break;
            case CardsName.agnes:
                description += "در هر نوبت 2 بار میتواند حمله کند \n".faConvert();
                break;
            case CardsName.JudyAbbott:
                break;
            case CardsName.minion1:
                break;
            case CardsName.minion2:
                break;
            case CardsName.minionMuscular:
                break;
            case CardsName.minionWolv:
                break;
            case CardsName.WildmuttBen:
                break;
            case CardsName.mushu:
                break;
            case CardsName.monky:
                break;
            case CardsName.leng:
                break;
            case CardsName.LeaderElsa:
                break;
            case CardsName.LeaderBatman:
                break;
            default:
                break;
        }

        if (description == string.Empty)
            description = "عادی".faConvert();

        return description;
    }
}
