﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionInfoContainer {

    public List<InteractionInfo> interactionInfos = new List<InteractionInfo>();

    public InteractionInfoContainer() { }
}
