﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand2GroundInfo {

    public Turn player;
    public int cardID;
    public int newPlayerMana;
    public CardInfo UpdatedCardInfo; // card info may be changed because of onstayGroundEffects
    public List<CardInfo> added2HandCard = new List<CardInfo>();
    public List<InteractionInfo> interactionInfoOnEnterGround = new List<InteractionInfo>();

    public Hand2GroundInfo() { }

    public Hand2GroundInfo(Turn myPlayer,CardInfo cardInfo,int acceptedCardID,int newMana)
    {
        player = myPlayer;
        UpdatedCardInfo = cardInfo;
        cardID = acceptedCardID;
        newPlayerMana = newMana;
    }
}
