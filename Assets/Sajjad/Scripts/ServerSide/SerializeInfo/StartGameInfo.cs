﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameInfo {

    public CardInfo leader1;
    public CardInfo leader2;
    public List<CardInfo> hand1;
    public List<CardInfo> hand2;

    public int mana1;
    public int mana2;

    public Turn currentTurn;
}
