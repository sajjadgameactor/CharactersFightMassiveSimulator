﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InteractionType
{
    Impact,
    Frozen,
    Heal,
    ExtraPower,
    RemoveStunEffect,
    RemoveHiddenSpecialAbility,
    MagicalImpact,
    SpiderWeb,
    Transform,
    NewCard,
}

public enum InteractionError
{
    None,
    VictamIsDead,
}
public class InteractionInfo {

    public InteractionError error = InteractionError.None;
    public InteractionType interactionType;
    public Turn subjectPlayer; 
    public int subjectID; // id of who do this interaction فاعل
    public List<int> objectIDList = new List<int>(); // id of who influence this interaction مفعول
    public int Power1;
    public int Power2;
    public bool subjectDead;
    public bool objectDead;
    public bool unHideSubject = true;
    public CardInfo subjectTransform;
    public CardInfo objectTransform;
    public List<CardInfo> newPlayersCard = new List<CardInfo>();

    //public List<InteractionInfo> sideInteraction = new List<InteractionInfo>();

    public InteractionInfo()
    { }

    public InteractionInfo(Turn subject, InteractionType type, int subID,int pow = -1,int objID = -1)
    {
        subjectPlayer = subject;
        interactionType = type;
        subjectID = subID;
        Power1 = pow;
        if(objID != -1)
            objectIDList.Add(objID);

    }

    //public InteractionInfo(Turn subject,int subID,int obID,int p,InteractionType type,bool vDead = false)
    //{
    //    subjectPlayer = subject; 
    //    subjectID = subID;
    //    objectIDList.Add(obID);
    //    Power = p;
    //    interactionType = type;
    //    objectDead = vDead;
    //}

    public InteractionInfo(InteractionError err)
    {
        error = err;
    }

    public void InitRemoveEffect(Turn Player,int objID,int attacker = -1)
    {
        interactionType = InteractionType.RemoveStunEffect;
        subjectPlayer = Player;
        objectIDList.Add(objID);
        subjectID = attacker;
    }

}
