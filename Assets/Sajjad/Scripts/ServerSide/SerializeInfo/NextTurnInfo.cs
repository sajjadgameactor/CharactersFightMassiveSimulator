﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextTurnInfo {

    public Turn currentTurn;
    public int currentTurnNumber;

    public int mana1;
    public int mana2;

    public List<CardInfo> newPlayer1Card;
    public List<CardInfo> newPlayer2Card;

    public List<InteractionInfo> interactionInfoAttacker;

}
