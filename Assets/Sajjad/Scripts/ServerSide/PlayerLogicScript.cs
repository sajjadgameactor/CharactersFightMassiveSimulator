﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public enum PlayerType
{
    Manual, AI, Network, LocalAI
}

public class PlayerLogicScript {
    public Dictionary<CardsName, int> newBalanceData = new Dictionary<CardsName, int>();

    public List<CardsName> playerDeck = new List<CardsName>();
    public List <CardInfo> handCard = new List<CardInfo>(); 
    public List<CardInfo> onGroundCard = new List<CardInfo>();

    public PlayerType playerType = PlayerType.Manual;

    public Turn myTurn { get; private set; }

    public CardInfo leader { get; private set; }

    int lastCardID = 0;

    int _Mana = 0;
    public int mana
    {
        get { return _Mana; }
        set
        {
            
            _Mana = Mathf.Min(value, 10);
        }
    }

    public PlayerLogicScript(Turn turn,int firstID,CardsName leaderName,PlayerType myType)
    {
        playerType = myType;

        myTurn = turn;
        lastCardID = firstID;
        leader = new CardInfo(leaderName, lastCardID++,myTurn);

        onGroundCard.Add(leader);
    }

    public CardInfo GetGroundCardInfo(int id)
    {
        //return handCard[id];
        return onGroundCard.FirstOrDefault(item => item.cardID == id);

    }

  

    private void AddChainedCard2Hand(CardInfo temp)
    {
        foreach (var item in temp.chainedCards)
        {
            Add2Hand(item);
        }
    }

    public CardInfo Add2Hand(CardsName card)
    {
        int cardID = lastCardID++;
        CardInfo temp = new CardInfo(card, cardID,myTurn);
        handCard.Add(temp);

        return temp;
    }

    public CardInfo FindHandCard(int id)
    {
        return handCard.FirstOrDefault(item => item.cardID == id);
    }

    public void Hand2Ground(CardInfo card)
    {
        mana -= card.mana;
        onGroundCard.Add(card);
        handCard.Remove(card);

        if(newBalanceData.ContainsKey(card.name))
        {
            newBalanceData[card.name]++;
        }
        else
        {
            newBalanceData.Add(card.name, 1);
        }
    }

    public CardInfo Deck2Hand()
    {
        CardInfo temp = Add2Hand(playerDeck[GameLogicScript.rnd.Next(playerDeck.Count)]);
        return temp;
    }
    public CardInfo TryDeck2HandHighMana(int value)
    {
        int i = 0;
        CardInfo best = new CardInfo(playerDeck[GameLogicScript.rnd.Next(playerDeck.Count)], -1, myTurn);
        while (i < 10) // we will try at most 10 time
        {
            CardInfo temp = new CardInfo(playerDeck[GameLogicScript.rnd.Next(playerDeck.Count)], -1, myTurn);

            if(temp.mana >= value)
            {
                best = temp;
                break;
            }
            else if(temp.mana > best.mana)
            {
                best = temp;
            }

            i++;
        }

        return Add2Hand(best.name);
    }

    public CardInfo TryDeck2HandLowMana(int value)
    {
        int i = 0;
        CardInfo best = new CardInfo(playerDeck[GameLogicScript.rnd.Next(playerDeck.Count)], -1, myTurn);
        while (i < 10) // we will try at most 10 time
        {
            CardInfo temp = new CardInfo(playerDeck[GameLogicScript.rnd.Next(playerDeck.Count)], -1, myTurn);

            if (temp.mana <= value)
            {
                best = temp;
                break;
            }
            else if (temp.mana < best.mana)
            {
                best = temp;
            }

            i++;
        }

        return Add2Hand(best.name);
    }

    public void RemoveGroundCard(CardInfo card)
    {
        onGroundCard.Remove(card);
    }

    public List<InteractionInfo> Go2NextTurn()
    {
        List<InteractionInfo> nextTurnInfo = new List<InteractionInfo>();
        InteractionInfo temp;
        foreach (var item in onGroundCard)
        {
            temp = item.Go2NextTurn();

            if (temp != null)
                nextTurnInfo.Add(temp);

        }

        return nextTurnInfo;
    }

    public bool CheckHasDefenderCard()
    {
        foreach (var item in onGroundCard)
        {
            if (item.isDefender)
                return true;
        }

        return false;
    }

    public CardInfo GetDefenderCard()
    {
        foreach (var item in onGroundCard)
        {
            if (item.isDefender)
                return item;
        }

        return null;
    }
    public bool HasGroundCapacity()
    {
        if (onGroundCard.Count < 7) //in logic leader is one card so 6 ground card plus leader = 7
        {
            return true;
        }

        return false;
    }

    public CardInfo FindRandomOnGroundCard()
    {
        if (onGroundCard.Count == 1) // only leader
            return null;

        int i = 0;
        while(i < 10) // try 10 time to find ground card
        {
            CardInfo temp = onGroundCard[GameLogicScript.rnd.Next(onGroundCard.Count)];
            if (temp != leader)
                return temp;

            i++;
        }

        Debug.LogWarning("unexpectly ground card not found!!!");

        return null; 
    }

    public CardInfo FindRandomOnGround()
    {
        return onGroundCard[GameLogicScript.rnd.Next(onGroundCard.Count)];
    }

    public List<CardInfo> FindAllCardLowerHealthThanValue(int value)
    {
        List<CardInfo> infoList = new List<CardInfo>();

        foreach (var item in onGroundCard)
        {
            if (item.currentHealth < value)
                infoList.Add(item);
        }

        return infoList;
    }

    public List<CardInfo> FindAllDamaged()
    {
        List<CardInfo> infoList = new List<CardInfo>();

        foreach (var item in onGroundCard)
        {
            if (item.IsDamaged() && item != leader)
                infoList.Add(item);
        }

        return infoList;
    }

    public CardInfo FindLowestManaCard()
    {
        CardInfo card = null;

        if (onGroundCard.Count == 1)
            return null;

        foreach (var item in onGroundCard)
        {
            if (item.mana == -1)
                continue;

            if (card == null)
                card = item;
            else if (item.mana < card.mana)
                card = item;
        }

        return card;
    }

    internal bool HasAttackerCard(out CardInfo card)
    {
        foreach (var item in onGroundCard)
        {
            if(item.CanAttack() && (item.power + item.extraPower) > 0)
            {
                card = item;
                return true;
            }
        }

        card = null;
        return false;
    }




    #region AI



    #endregion
}
