﻿using BestHTTP;
using LitJson;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class MassiveLogicTester:MonoBehaviour
{
    public static List<ItemMother> balancedItemList = new List<ItemMother>();

    [SerializeField]
    Button startButton;

    [SerializeField]
    InputField inputField;

    [SerializeField]
    Text doneNumberText, donePercentText;

    [SerializeField]
    Image fillerImage;

    void Start()
    {
        startButton.enabled = true;

        Application.runInBackground = true;
        QualitySettings.vSyncCount = 1;
        GetActiveVersion();

    }

    void GetGameData()
    {
        try
        {
            HTTPRequest request = new HTTPRequest(new Uri(Constant.getBalanceDataURL), HTTPMethods.Get, OnRequestFinished);
            //request.SetHeader("version", (Constant.groupVersion -1).ToString());
            request.Send();
        }
        catch
        {
            Debug.LogWarning("Request Data Problem");
        }
       


    }

    void OnRequestFinished(HTTPRequest request, HTTPResponse response)
    {
        JSONNode tempNode;

        if (request.Response != null && request.Response.IsSuccess)
        {
            string ss = request.Response.DataAsText;
            tempNode = JSONNode.Parse(ss);
            balancedItemList = JsonMapper.ToObject<List<ItemMother>>(request.Response.DataAsText);


            startButton.enabled = true;


        }
        else
        {
            Debug.LogWarning("server problem");
        }


    }

    int activeVersion = 0;
    void GetActiveVersion()
    {
        HTTPRequest request = new HTTPRequest(new Uri(Constant.getActiveVersionURL), HTTPMethods.Get, OnRequestActiveVersionFinished);
        request.Send();


    }

    void OnRequestActiveVersionFinished(HTTPRequest request, HTTPResponse response)
    {
        if(request.Response != null && request.Response.IsSuccess)
        {
            activeVersion = int.Parse(request.Response.DataAsText);
            GetGameData();
        }
        else
        {
            activeVersion = -1;
        }
     

    }

    int gameRequestNumber;
    public void OnStartMassiveRequest()
    {
        gameRequestNumber = int.Parse(inputField.text);
        startButton.enabled = false;
        startButton.GetComponent<Image>().color = Color.red;
        fillerImage.fillAmount = 0;
        sendedNumber = 0;
        doneNumberText.text = "0";
        donePercentText.text = "0%";
        StartCoroutine(MassiveGameStart());

    }

    private IEnumerator MassiveGameStart()
    {
        int i = 0;
        while (i < gameRequestNumber)
        {
            GameLogicScript gameLogic = new GameLogicScript();
            gameLogic.OnGameEnd += GameLogic_OnGameEnd;
            gameLogic.StartClientGame();

            i++;

            //if (i % 10 == 0)
            //{
            //    doneNumberText.text = 
            //}

            if (i - sendedNumber > 15)
                yield return new WaitForSeconds(0.1f);
        }

        while (i - sendedNumber > 3)
        {
            Debug.Log("Wait for send data");
            yield return new WaitForSeconds(1);

        }

        yield return new WaitForSeconds(1);

        ProcessDone();

          yield break;
    }

    void ProcessDone()
    {
        startButton.enabled = true;
        startButton.GetComponent<Image>().color = Color.green;
    }

    private void GameLogic_OnGameEnd(GameLogicScript gameLogic)
    {
        if (activeVersion != -1)
        {
            Thread newThread = new Thread(() => SendGameData(gameLogic));
            newThread.Start();
        }
        else
        {
            Debug.Log(gameLogic.currentTurn.ToString() + " Win");
        }
      
    }

    void SendGameData(GameLogicScript gameLogic)
    {
        PlayerLogicScript winnerPlayer, loserPlayer;

        winnerPlayer = gameLogic.attackerPlayer;
        loserPlayer = gameLogic.defenderPlayer;
        

        int gameMultiply = 1;
        if (winnerPlayer.leader.currentHealth > 25)
            gameMultiply = 5;
        else if (winnerPlayer.leader.currentHealth > 20)
            gameMultiply = 4;
        else if (winnerPlayer.leader.currentHealth > 15)
            gameMultiply = 3;
        else if (winnerPlayer.leader.currentHealth > 10)
            gameMultiply = 2;
        else
            gameMultiply = 1;

        JSONNode jsonRoot = new JSONClass();
        foreach (var item in winnerPlayer.newBalanceData)
        {
            string id = FindItemBalancedID(item.Key);
            if (string.Equals(id, "No"))
            {
                continue;
            }

            jsonRoot[id]["win"] = (item.Value * gameMultiply).ToString();
        }
        foreach (var item in loserPlayer.newBalanceData)
        {
            string id = FindItemBalancedID(item.Key);
            if (string.Equals(id, "No"))
            {
                continue;
            }

            jsonRoot[id]["lose"] = (item.Value * gameMultiply).ToString();
        }


        string sss = jsonRoot.ToString();

        HTTPRequest request = new HTTPRequest(new Uri(Constant.sendBalanceDataURL), HTTPMethods.Post, OnFinishSendData);
        request.SetHeader("version", activeVersion.ToString());
        request.SetHeader("Content-Type", "application/json");
        request.RawData = Encoding.UTF8.GetBytes(sss);
        request.Send();

    }

    static int sendedNumber = 0;
    void OnFinishSendData(HTTPRequest request, HTTPResponse response)
    {
        if (response.IsSuccess)
        {
            ++sendedNumber;
            if (sendedNumber% 10 == 0)
            {
                //Debug.Log("Done: " + sendedNumber);
                doneNumberText.text = sendedNumber.ToString();
                float donePercent = ((float)sendedNumber / gameRequestNumber);
                fillerImage.fillAmount = donePercent;
                donePercentText.text = (donePercent * 100).ToString("f1") + "%";
            }
        }
        else
        {
            Debug.Log("faild to send data");
        }
    }

    static string FindItemBalancedID(CardsName name)
    {
        foreach (var item in balancedItemList)
        {
            string nameString = name.ToString();

            if (string.Equals(nameString, item.items.name))
            {
                return item.items.id;
            }
        }

        Debug.LogError("Id not found: " + name.ToString());
        return "No";
    }
}
