﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct AttackMessegeScript {

    float attackPower;
    int attackCardID;
    int mainVictamID;
    Dictionary<int, float> sideVictam; // id - attackPower
}
