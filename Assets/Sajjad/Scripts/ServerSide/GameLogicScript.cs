﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using LitJson;

using Random = System.Random;
using SimpleJSON;
using BestHTTP;
using System.Text;
using System.Threading;



public enum GameLogicState
{
    player1Turn, Player2Turn
}

public class GameLogicScript
{
    public static Random rnd = new System.Random();

    public delegate void GameEndEventHandler(GameLogicScript gameLogic);
    public event GameEndEventHandler OnGameEnd;
    private void GameEndEvent()
    {
        //OnPushPoint?.Invoke(CurrentScore);
        if (OnGameEnd != null)
        {
            OnGameEnd(this);
        }
    }


    //public delegate void EnterGround(GameLogicScript game);
    //public event EnterGround EnterGroundEvent;
    //private void OnEnterGround()
    //{
    //    //OnPushPoint?.Invoke(CurrentScore);
    //    if (EnterGroundEvent != null)
    //    {
    //        EnterGroundEvent(this);
    //    }
    //}

    public PlayerLogicScript player1, player2, attackerPlayer, defenderPlayer;
    public Turn currentTurn;
    public int currentTurnNumber = -1;

    public bool isGameEnd = false;

    //TurnManager turnManager = new TurnManager();

    JsonWriter jsonWriter = new JsonWriter();

    List<InteractionEvent> stayOnGroundEffects = new List<InteractionEvent>();
    List<InteractionEvent> OnEachTurnInteractionEffectsPlayer1 = new List<InteractionEvent>();
    List<InteractionEvent> OnEachTurnInteractionEffectsPlayer2 = new List<InteractionEvent>();

    public GameLogicScript()
    {
        Turn firstTurn = Turn.Player1;


        player1 = new PlayerLogicScript(Turn.Player1, 0, CardsName.LeaderBatman, PlayerType.LocalAI);
        player2 = new PlayerLogicScript(Turn.Player2, 1000, CardsName.LeaderElsa, PlayerType.LocalAI);

        currentTurn = firstTurn;

        InitDeck(Turn.Player1);
        InitDeck(Turn.Player2);

        SetFirstPlayerHand();


    }

    public void StartClientGame()
    {
        OnTurnChange(Turn.Player1);

        if (attackerPlayer.playerType == PlayerType.LocalAI)
        {
            HandleAttackerByAI();
        }

        //StartGameInfo startInfo = new StartGameInfo();
        //startInfo.leader1 = player1.leader;
        //startInfo.hand1 = player1.handCard;
        //startInfo.mana1 = player1.mana;

        //startInfo.leader2 = player2.leader;
        //startInfo.hand2 = player2.handCard;
        //startInfo.mana2 = player2.mana;

        //startInfo.currentTurn = currentTurn;

        //string startInfoJson = JsonMapper.ToJson(startInfo);
        //GameSceneScript.Instance.StartGame(startInfoJson);

    }

    public bool InitDeck(Turn player)
    {
        PlayerLogicScript currentPlayer = player1;
        switch (player)
        {
            case Turn.Player1:
                currentPlayer = player1;
                break;
            case Turn.Player2:
                currentPlayer = player2;
                break;
            default:
                break;
        }

        //string deckString = "";

        //switch (player)
        //{
        //    case Turn.Player1:
        //        deckString = PlayerPrefs.GetString(Constant.player1Key + Constant.deckKey);
        //        break;
        //    case Turn.Player2:
        //        deckString = PlayerPrefs.GetString(Constant.player2Key + Constant.deckKey);
        //        break;
        //    default:
        //        break;
        //}

        //if (deckString != string.Empty)
        //{
        //    char[] arrOperators = { ' ' };
        //    string[] selectedCardNameInt = deckString.Split(arrOperators, StringSplitOptions.RemoveEmptyEntries);

        //    try
        //    {
        //        if (selectedCardNameInt.Length != 0)
        //        {
        //            foreach (var item in selectedCardNameInt)
        //            {
        //                int temp = int.Parse(item);

        //                currentPlayer.playerDeck.Add((CardsName)temp);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Debug.LogError(e.Message);
        //    }
        //}
        //else
        {
            InitRandomDeck(currentPlayer);
        }

        return false;
    }
    void InitRandomDeck(PlayerLogicScript currentPlayer)
    {
        currentPlayer.playerDeck.Add(CardsName.Olaf);
        currentPlayer.playerDeck.Add(CardsName.tom);
        currentPlayer.playerDeck.Add(CardsName.jerry);
        currentPlayer.playerDeck.Add(CardsName.minion1);
        currentPlayer.playerDeck.Add(CardsName.minion2);
        for (int i = 0; i < 25; i++)
        {
            currentPlayer.playerDeck.Add((CardsName)rnd.Next(1, 56));
        }

       // Debug.LogWarning("Randomly Init Deck of " + currentPlayer.myTurn.ToString());
    }
    void SetFirstPlayerHand()
    {
        //var randomNumbers = Enumerable.Range(1, 20).OrderBy(x => rnd.Next()).Take(4).ToList();

        for (int i = 0; i < 4; i++)
        {
            player1.TryDeck2HandLowMana(2);
            player2.TryDeck2HandLowMana(2);

        }

        //player1.Add2Hand(CardsName.JudyAbbott);
        
        //player2.Add2Hand(CardsName.benten);

    }

    public void Hand2GroundRequest(int cardID, Turn applicantPlayer)
    {
        if (AuthenticateApplicantPlayer(applicantPlayer))
        {
            CardInfo temp = attackerPlayer.FindHandCard(cardID);

            if (!attackerPlayer.HasGroundCapacity())
            {
                throw new System.Exception("ground card is full");
            }
            if (temp == null)
            {
                throw new System.Exception("Card not found in hand");
            }
            if (temp.mana > attackerPlayer.mana)
            {
                Debug.LogError("Not enough mana");

            }

            attackerPlayer.Hand2Ground(temp);

            Hand2GroundInfo hand2GroundInfo = new Hand2GroundInfo(attackerPlayer.myTurn, temp, temp.cardID, attackerPlayer.mana);

            foreach (var item in temp.chainedCards)
            {
                hand2GroundInfo.added2HandCard.Add(attackerPlayer.Add2Hand(item));
            }
            foreach (var item in temp.OnEachTurnInteractions)
            {
                switch (item.subjectPlayer)
                {
                    case Turn.Player1:
                        OnEachTurnInteractionEffectsPlayer1.Add(item);

                        break;
                    case Turn.Player2:
                        OnEachTurnInteractionEffectsPlayer2.Add(item);
                        break;
                    default:
                        break;
                }
            }

            List<InteractionInfo> infoList;
            foreach (var item in temp.OnEnterGroundInteractions)
            {
                infoList = ProcessInteractionEvent(item);
                if (infoList != null)
                    hand2GroundInfo.interactionInfoOnEnterGround.AddRange(infoList);
            }

            foreach (var item in stayOnGroundEffects) // all old effect added on this card
            {
                var info = ProcessStayOnGroundEffect(item, temp, false);
                if (info != null)
                    hand2GroundInfo.interactionInfoOnEnterGround.Add(info);
            }

            foreach (var item in temp.OnStayGroundInteractions)
            {
                stayOnGroundEffects.Add(item);

                hand2GroundInfo.interactionInfoOnEnterGround.AddRange(FillInteractionData4StayOnGround(item));
            }

            //string jsonHandInfo = JsonMapper.ToJson(hand2GroundInfo);
            //GameSceneScript.Instance.Hand2GroundAcceptedByServer(jsonHandInfo);


        }

    }

    List<InteractionInfo> FillInteractionData4StayOnGround(InteractionEvent interactionEvent, bool reverse = false)
    {
        InteractionInfo info;
        List<InteractionInfo> fillList = new List<InteractionInfo>();
        foreach (var card in player1.onGroundCard)
        {
            info = ProcessStayOnGroundEffect(interactionEvent, card, reverse);
            if (info != null)
                fillList.Add(info);
        }
        foreach (var card in player2.onGroundCard)
        {
            info = ProcessStayOnGroundEffect(interactionEvent, card, reverse);
            if (info != null)
                fillList.Add(info);
        }

        return fillList;
    }

    InteractionInfo ProcessStayOnGroundEffect(InteractionEvent item, CardInfo card, bool reverse)
    {
        InteractionInfo temp;
        if (IsEventAffectedCard(item, card))
        {
            switch (item.interactionType)
            {
                case InteractionType.Impact:
                    break;
                case InteractionType.Frozen:
                    break;
                case InteractionType.Heal:
                    break;
                case InteractionType.ExtraPower:
                    if (reverse)
                    {
                        card.extraPower -= item.power;
                        temp = new InteractionInfo(item.subjectPlayer, item.interactionType, item.subjectID, -item.power);
                    }
                    else
                    {
                        card.extraPower += item.power;
                        temp = new InteractionInfo(item.subjectPlayer, item.interactionType, item.subjectID, item.power);

                    }

                    temp.objectIDList.Add(card.cardID);
                    return temp;
                case InteractionType.RemoveStunEffect:
                    break;
                case InteractionType.RemoveHiddenSpecialAbility:
                    break;
                default:
                    break;
            }
        }

        return null;
    }

    private bool IsEventAffectedCard(InteractionEvent item, CardInfo card)
    {
        switch (item.interactionAffected)
        {
            case InteractionAffected.AllEnemyCard:
                if (item.subjectPlayer == card.myPlayer)
                    return false;
                else
                    return true;
            case InteractionAffected.AllTeammateCard:
                if (item.subjectPlayer == card.myPlayer)
                    return true;
                else
                    return false;
            default:
                throw new Exception("Not implemented");
        }
    }

    public void AttackCardRequest(int attackerID, int victamID, Turn applicantPlayer)
    {

        if (AuthenticateApplicantPlayer(applicantPlayer))
        {
            List<InteractionInfo> intractions = new List<InteractionInfo>();
            intractions.AddRange(ProcessInteractionEvent(new InteractionEvent(applicantPlayer, InteractionType.Impact, InteractionAffected.DefinedEnemy, attackerID, -1, victamID)));

            //string jsonInteration = JsonMapper.ToJson(intractions);
            //GameSceneScript.Instance.AttackAcceptedByServer(jsonInteration);
        }

    }

    List<InteractionInfo> ProcessInteractionEvent(InteractionEvent interactionEvent)
    {
        PlayerLogicScript interactionAttacker;
        PlayerLogicScript interactionDefender;

        switch (interactionEvent.subjectPlayer)
        {
            case Turn.Player1:
                interactionAttacker = player1;
                interactionDefender = player2;
                break;
            case Turn.Player2:
                interactionAttacker = player2;
                interactionDefender = player1;
                break;
            default:
                throw new Exception("What happen !!!");
        }


        List<CardInfo> target = new List<CardInfo>();
        CardInfo tempCard = null;
        switch (interactionEvent.interactionAffected)
        {
            case InteractionAffected.None:
                break;
            case InteractionAffected.Self:
                target.Add(interactionAttacker.GetGroundCardInfo(interactionEvent.subjectID));
                break;
            case InteractionAffected.LeaderEnemy:
                target.Add(interactionDefender.leader);
                break;
            case InteractionAffected.LeaderTeammate:
                target.Add(interactionAttacker.leader);
                break;
            case InteractionAffected.DefinedEnemy:
                target.Add(interactionDefender.GetGroundCardInfo(interactionEvent.objectID));
                break;
            case InteractionAffected.DefinedTeammate:
                target.Add(interactionAttacker.GetGroundCardInfo(interactionEvent.objectID));
                break;
            case InteractionAffected.RandomEnemyGroundCard:
                tempCard = interactionDefender.FindRandomOnGroundCard();
                if (tempCard != null)
                    target.Add(tempCard);
                break;
            case InteractionAffected.RandomTeammateGroundCard:
                tempCard = interactionAttacker.FindRandomOnGroundCard();
                if (tempCard != null)
                    target.Add(tempCard);
                break;
            case InteractionAffected.RandomEnemyGround:
                tempCard = interactionDefender.FindRandomOnGround();
                if (tempCard != null)
                    target.Add(tempCard);
                break;
            case InteractionAffected.RandomTeammateGround:
                tempCard = interactionAttacker.FindRandomOnGround();
                if (tempCard != null)
                    target.Add(tempCard);
                break;
            case InteractionAffected.AllEnemyCard:
                target = interactionDefender.onGroundCard;
                break;
            case InteractionAffected.AllTeammateCard:
                target = interactionAttacker.onGroundCard;
                break;
            case InteractionAffected.AllEnemyLowerHealthThanVariable:
                target = interactionDefender.FindAllCardLowerHealthThanValue(interactionEvent.variable1);
                break;
            case InteractionAffected.AllDamaged:
                target = interactionDefender.FindAllDamaged();
                break;
            case InteractionAffected.LowestManaEnemy:
                tempCard = interactionDefender.FindLowestManaCard();
                if (tempCard != null)
                    target.Add(tempCard);
                break;
            default:
                throw new Exception("Unexpected affected type");
        }

        if (target == null)
        {
            Debug.LogWarning("null target");
            return null;
        }
        else
        {
            InteractionInfo info;
            List<InteractionInfo> interactionInfoList = new List<InteractionInfo>();
            switch (interactionEvent.interactionType)
            {
                case InteractionType.Impact:
                    CardInfo attackerCard = interactionAttacker.GetGroundCardInfo(interactionEvent.subjectID);

                    foreach (var targetObject in target)
                    {
                        CardInfo defenderCard = interactionDefender.GetGroundCardInfo(interactionEvent.objectID);

                        CheckCanAttack(attackerCard);
                        CheckDefenderCard(defenderCard);
                        attackerCard.currentTurnAttackNumber--;

                        interactionInfoList.AddRange(ImpactProcess(interactionEvent, interactionAttacker, attackerCard, interactionDefender, defenderCard));

                    }
                    break;
                case InteractionType.MagicalImpact:
                    info = new InteractionInfo(interactionEvent.subjectPlayer, interactionEvent.interactionType, interactionEvent.subjectID, interactionEvent.power);
                    foreach (var item in target)
                    {
                        item.Impact(interactionEvent.power);
                        info.objectIDList.Add(item.cardID);
                    }
                    interactionInfoList.Add(info);
                    return interactionInfoList;
                case InteractionType.Frozen:
                    info = new InteractionInfo(interactionEvent.subjectPlayer, interactionEvent.interactionType, interactionEvent.subjectID, interactionEvent.power);
                    foreach (var item in target)
                    {
                        item.Stun(interactionEvent.power);
                        info.objectIDList.Add(item.cardID);
                    }
                    interactionInfoList.Add(info);
                    return interactionInfoList;
                case InteractionType.SpiderWeb:
                    info = new InteractionInfo(interactionEvent.subjectPlayer, interactionEvent.interactionType, interactionEvent.subjectID, interactionEvent.power);
                    foreach (var item in target)
                    {
                        item.Stun(interactionEvent.power);
                        info.objectIDList.Add(item.cardID);
                    }
                    interactionInfoList.Add(info);
                    return interactionInfoList;
                case InteractionType.Heal:
                    info = new InteractionInfo(interactionEvent.subjectPlayer, interactionEvent.interactionType, interactionEvent.subjectID, interactionEvent.power);
                    foreach (var item in target)
                    {
                        if (item.IsDamaged())
                        {
                            item.Heal(interactionEvent.power);
                            info.objectIDList.Add(item.cardID);
                        }

                    }
                    interactionInfoList.Add(info);
                    return interactionInfoList;
                case InteractionType.RemoveHiddenSpecialAbility:
                    info = new InteractionInfo(interactionEvent.subjectPlayer, interactionEvent.interactionType, interactionEvent.subjectID);
                    foreach (var item in target)
                    {
                        if (item.isHidden)
                        {
                            item.isHidden = false;
                            info.objectIDList.Add(item.cardID);
                        }
                    }
                    interactionInfoList.Add(info);
                    return interactionInfoList;
                case InteractionType.ExtraPower:
                    info = new InteractionInfo(interactionEvent.subjectPlayer, interactionEvent.interactionType, interactionEvent.subjectID, interactionEvent.power);
                    foreach (var item in target)
                    {
                        item.extraPower += interactionEvent.power;
                        info.objectIDList.Add(item.cardID);
                    }
                    interactionInfoList.Add(info);
                    return interactionInfoList;
                case InteractionType.Transform:
                    info = new InteractionInfo(interactionEvent.subjectPlayer, interactionEvent.interactionType, interactionEvent.subjectID);
                    foreach (var item in target)
                    {
                        item.TransformCard((CardsName)interactionEvent.power);
                        info.objectTransform = item;
                        info.objectIDList.Add(item.cardID);
                    }
                    interactionInfoList.Add(info);
                    return interactionInfoList;
                case InteractionType.NewCard:
                    info = new InteractionInfo(interactionEvent.subjectPlayer, interactionEvent.interactionType, interactionEvent.subjectID);
                    CardInfo card;
                    for (int i = 0; i < interactionEvent.power; i++)
                    {
                        switch (interactionEvent.variable1)
                        {
                            case 0: // random
                                card = interactionAttacker.Deck2Hand();
                                break;
                            case 1: // bigger mana than
                                card = interactionAttacker.TryDeck2HandHighMana(interactionEvent.variable2);
                                break;
                            default:
                                card = interactionAttacker.Deck2Hand();
                                break;
                        }
                        info.newPlayersCard.Add(card);
                    }
                    interactionInfoList.Add(info);
                    break;
                default:
                    throw new Exception("undefined intraction type");
            }


            return interactionInfoList;
        }

    }

    private List<InteractionInfo> ImpactProcess(InteractionEvent interactionEvent, PlayerLogicScript interactionAttacker, CardInfo attackerCard, PlayerLogicScript interactionDefender, CardInfo defenderCard)
    {
        int currentAttackerPower = CalculateImpactPower(attackerCard);
        int currentDefenderPower = CalculateImpactPower(defenderCard);
        InteractionInfo attackInfo = new InteractionInfo(interactionAttacker.myTurn, interactionEvent.interactionType, attackerCard.cardID, currentAttackerPower, defenderCard.cardID);
        attackInfo.Power2 = currentDefenderPower;
        List<InteractionInfo> interactionInfoList = new List<InteractionInfo>();

        if (attackerCard.isHidden)
        {
            attackerCard.isHidden = false;
            attackInfo.unHideSubject = true;
        }

        if (defenderCard.Impact(currentAttackerPower))
        {
            attackInfo.objectDead = true;
            if (defenderCard.deadTransform == CardsName.None)
            {
                defenderPlayer.RemoveGroundCard(defenderCard);

                if (defenderCard.name == CardsName.LeaderBatman || defenderCard.name == CardsName.LeaderElsa)
                {
                    isGameEnd = true;
                    //Debug.Log(attackerPlayer.myTurn.ToString() + " Win");
                    GameEndEvent();
                   
                }
            }
            else
            {
                defenderCard.TransformCard(defenderCard.deadTransform);
                attackInfo.objectTransform = defenderCard;
            }
            interactionInfoList.AddRange(RemoveCardProcess(interactionDefender, defenderCard, attackInfo));

        }
        if (defenderCard != defenderPlayer.leader)
        {
            if (attackerCard.Impact(currentDefenderPower))
            {
                attackInfo.subjectDead = true;
                if (attackerCard.deadTransform == CardsName.None)
                    interactionAttacker.RemoveGroundCard(attackerCard);
                else
                {
                    attackerCard.TransformCard(attackerCard.deadTransform);
                    attackInfo.subjectTransform = attackerCard;
                }

                interactionInfoList.AddRange(RemoveCardProcess(interactionAttacker, attackerCard, attackInfo));

            }

        }

        interactionInfoList.Add(attackInfo);

        return interactionInfoList;
    }

    private List<InteractionInfo> RemoveCardProcess(PlayerLogicScript interactionPlayer, CardInfo deadCard, InteractionInfo impactInfo)
    {
        List<InteractionInfo> sideInfo = new List<InteractionInfo>();
        //Debug.Log("remove card: " + deadCard.name);

        sideInfo.AddRange(RemoveEffects(deadCard));

        if (deadCard.chainedCardsOnExit.Count != 0)
        {
            foreach (var item in deadCard.chainedCardsOnExit)
            {

                impactInfo.newPlayersCard.Add(defenderPlayer.Add2Hand(item));
            }
        }

        foreach (var item in deadCard.OnExitGroundInteractions)
        {
            sideInfo.AddRange(ProcessInteractionEvent(item));
        }

        return sideInfo;
    }

    int CalculateImpactPower(CardInfo attacker)
    {
        if (attacker.criticalChance != 0)
        {
            if ((float)rnd.Next(100) / 100 < attacker.criticalChance)
            {
                return Mathf.RoundToInt((attacker.power + attacker.extraPower) * (float)attacker.criticalX);
            }
        }


        return attacker.power + attacker.extraPower;

    }

    private List<InteractionInfo> RemoveEffects(CardInfo removedCard)
    {
        List<InteractionInfo> interactionList = new List<InteractionInfo>();
        if (removedCard.OnStayGroundInteractions.Count != 0)
        {
            foreach (var item in removedCard.OnStayGroundInteractions)
            {
                stayOnGroundEffects.Remove(item);
                interactionList.AddRange(FillInteractionData4StayOnGround(item, true));

            }
        }
        if (removedCard.OnEachTurnInteractions.Count != 0)
        {
            foreach (var item in removedCard.OnEachTurnInteractions)
            {
                switch (item.subjectPlayer)
                {
                    case Turn.Player1:
                        OnEachTurnInteractionEffectsPlayer1.Remove(item);
                        break;
                    case Turn.Player2:
                        OnEachTurnInteractionEffectsPlayer2.Remove(item);
                        break;
                    default:
                        break;
                }

            }
        }

        return interactionList;
    }

    /// <summary>
    /// check if defender player has defender card on ground only those card can be attacked
    /// </summary>
    /// <param name="defenderCard"></param>
    private void CheckDefenderCard(CardInfo defenderCard)
    {
        if (defenderCard == null)
            Debug.LogError("null defender"); // may be happen during animation time

        if (defenderCard.isHidden)
            Debug.LogError("Hidden Card");


        if (defenderCard.isDefender)
            return;

        if (defenderPlayer.CheckHasDefenderCard())
        {
            Debug.LogError("Only can attack defender card");
        }
    }

    void CheckCanAttack(CardInfo card)
    {
        if (!card.CanAttack())
        {
            Debug.LogError("attacker Card can not attack");

            // AttackRejectedByServer(messege);
        }

    }

    bool AuthenticateApplicantPlayer(Turn applicantPlayer)
    {
        if (currentTurn == applicantPlayer)
        {
            return true;
        }
        else
        {
            Debug.LogError("Wrong Turn");
            return false;
        }
    }

    public void TurnDone(Turn applicantPlayer)
    {
        List<CardInfo> newCardPlayer1 = new List<CardInfo>(), newCardPlayer2 = new List<CardInfo>();
        List<InteractionEvent> currentOnEachTurn;
        if (AuthenticateApplicantPlayer(applicantPlayer))
        {
            if (currentTurn == Turn.Player1)
            {
                currentTurn = Turn.Player2;
                currentOnEachTurn = OnEachTurnInteractionEffectsPlayer2;
                newCardPlayer2.Add(player2.Deck2Hand());
                if (player2.handCard.Count < 4)
                    newCardPlayer2.Add(player2.Deck2Hand());
            }
            else
            {
                currentTurn = Turn.Player1;

                currentOnEachTurn = OnEachTurnInteractionEffectsPlayer1;

                newCardPlayer1.Add(player1.Deck2Hand());
                if (player1.handCard.Count < 4)
                    newCardPlayer1.Add(player1.Deck2Hand());
            }
            OnTurnChange(currentTurn);

            List<InteractionInfo> interactionList = attackerPlayer.Go2NextTurn();

            foreach (var item in currentOnEachTurn)
            {
                interactionList.AddRange(ProcessInteractionEvent(item));
            }


            if (attackerPlayer.playerType == PlayerType.LocalAI)
            {
                HandleAttackerByAI();
            }

            //NextTurnInfo info = new NextTurnInfo();
            //info.currentTurn = currentTurn;
            //info.currentTurnNumber = currentTurnNumber;
            //info.mana1 = player1.mana;
            //info.mana2 = player2.mana;
            //info.newPlayer1Card = newCardPlayer1;
            //info.newPlayer2Card = newCardPlayer2;
            //info.interactionInfoAttacker = interactionList;

            //GameSceneScript.Instance.InitNewTurn(JsonMapper.ToJson(info));
        }
        else
        {
            Debug.LogError("Wrong Player");
        }
    }

    private void OnTurnChange(Turn currentTurn)
    {
        switch (currentTurn)
        {
            case Turn.Player1:
                attackerPlayer = player1;
                defenderPlayer = player2;
                break;
            case Turn.Player2:
                attackerPlayer = player2;
                defenderPlayer = player1;
                break;
            default:
                break;
        }
        currentTurnNumber++;

        attackerPlayer.mana += ((int)((float)currentTurnNumber + 1) / 2) + 1;

      

    }

    void HandleAttackerByAI()
    {

        for (int i = 0; i < attackerPlayer.handCard.Count; i++)
        {
            if (!attackerPlayer.HasGroundCapacity())
                break;

            if (attackerPlayer.handCard[i].mana <= attackerPlayer.mana)
            {
                Hand2GroundRequest(attackerPlayer.handCard[i].cardID,attackerPlayer.myTurn);
                i--;
            }
        }
        CardInfo attackCard;
        while (attackerPlayer.HasAttackerCard(out attackCard))
        {
            AIAttack(attackCard);

            if (isGameEnd)
                return;
        }

        //GameSceneScript.Instance.OnTurnChangeRequest();
        TurnDone(attackerPlayer.myTurn);
    }

    public void AIAttack(CardInfo attacker)
    {
        CardInfo victamCard = defenderPlayer.leader;

        if (defenderPlayer.CheckHasDefenderCard())
        {
            victamCard = defenderPlayer.GetDefenderCard();
        }
        else if(CanKillLeaderWithAllAttack())
        {
            victamCard = defenderPlayer.leader;
        }
        else // find more valuable card
        {
            foreach (var defenderCard in defenderPlayer.onGroundCard)
            {
                if (CanKillDefender(attacker, defenderCard) &&
                    (DefenderHasMorePower(attacker, defenderCard) ||
                       AttackerHasMoreHealth(attacker, defenderCard) && PowerNotWasted(attacker, defenderCard)) &&
                    !defenderCard.isHidden)
                {
                    victamCard = defenderCard;
                    break;
                }
            }



        }

        if(victamCard == defenderPlayer.leader) // special cards process
        {
            CardInfo specialVictam = null;
            if (attacker.name == CardsName.wall_E && attackerPlayer.leader.currentHealth < 27)
            {
                specialVictam = FindKillerVictam(attacker);
                if (specialVictam != null)
                    victamCard = specialVictam;
            }
            else if(attacker.name == CardsName.JackSparrow)
            {
                foreach (var item in defenderPlayer.onGroundCard)
                {
                    if(item.mana > 5 && item.currentHealth > 4)
                    {
                        specialVictam = FindKillerVictam(attacker);
                        if (specialVictam != null)
                            victamCard = specialVictam;
                    }
                }

            }
        }
       

        AttackCardRequest(attacker.cardID, victamCard.cardID, attacker.myPlayer);

    }

    private bool CanKillLeaderWithAllAttack()
    {
        int maxPower = 0;
        foreach (var item in attackerPlayer.onGroundCard)
        {
            int attackNumber = 0;
            while (attackNumber < item.currentTurnAttackNumber)
            {
                maxPower += item.GetCurrentPower();
                attackNumber++;
            }
        }

        if (maxPower >= defenderPlayer.leader.currentHealth)
            return true;
        else
            return false;
    }

    private CardInfo FindKillerVictam(CardInfo attacker)
    {
        foreach (var item in defenderPlayer.onGroundCard)
        {
            if (item.power >= attacker.currentHealth && !item.isHidden)
               return item;
        }

        return null;
    }

    private static bool PowerNotWasted(CardInfo attacker, CardInfo defenderCard)
    {
        return attacker.GetCurrentPower() == defenderCard.currentHealth;
    }

    private static bool AttackerHasMoreHealth(CardInfo attacker, CardInfo defenderCard)
    {
        return attacker.currentHealth > defenderCard.currentHealth;
    }

    private static bool DefenderHasMorePower(CardInfo attacker, CardInfo defenderCard)
    {
        return defenderCard.GetCurrentPower() > attacker.GetCurrentPower();
    }

    private static bool CanKillDefender(CardInfo attacker, CardInfo defenderCard)
    {
        return defenderCard.currentHealth <= attacker.GetCurrentPower();
    }

  

  

   

}
