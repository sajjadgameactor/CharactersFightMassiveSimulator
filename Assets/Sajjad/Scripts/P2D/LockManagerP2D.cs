﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum LocksName
{
    Chapter1Lock,Chapter1SpecialLock, Chapter2Lock, Chapter2SpecialLock, Chapter3Lock, Chapter3SpecialLock, Chapter4Lock, Chapter4SpecialLock,
    CharacterNormal,CharacterFat, CharacterMashti, CharacterMortaz, CharacterBacket, CharacterChina,
    UpdateCharacterNormal, UpdateCharacterFat, UpdateCharacterMashti, UpdateCharacterMortaz, UpdateCharacterBacket, UpdateCharacterChina,
}

public class LockManagerP2D {

    public static Dictionary<LocksName, LockInfo> Locks = new Dictionary<LocksName, LockInfo>();
    public static void InitLocks()
    {

        //Locks.Add(LocksName.Chapter1Lock, new LockInfo(LocksName.Chapter1Lock, 0, SettingP2D.ChapterScene, "فصل اول",0));
        //Locks.Add(LocksName.Chapter1SpecialLock, new LockInfo(LocksName.Chapter1SpecialLock, 45, SettingP2D.ChapterScene, "مرحله ویژه فصل اول"));


#if UNITY_EDITOR

        LockAll();
#endif
    }


    private static void LockAll()
    {
        foreach (var item in Locks)
        {
            item.Value.Unlock = 0;
        }
    }

}
