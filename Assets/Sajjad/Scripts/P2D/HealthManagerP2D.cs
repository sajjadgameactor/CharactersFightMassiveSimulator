﻿using System;
using System.Collections.Generic;

public delegate void HealthOverEventHandler(object sender);

public class HealthManagerP2D
{
    private float currentHealth;
    private float _maxHealth;


    public event HealthOverEventHandler HealthOver;

    private void OnHealthOver(object sender)
    {
        if (HealthOver != null)
        {
            HealthOver(sender);
        }
    }


    public float GetHealth
    {
        get { return currentHealth; }
            

    }
    public float MaxHealth
    {
        get { return _maxHealth; }
        set
        {
            _maxHealth = value;
            if (currentHealth > _maxHealth)
                currentHealth = _maxHealth;
        }
    }

    public void Damage(float damageAmount)
    {
        
        if (!isHelthOver)
        {
            currentHealth -= damageAmount;

            if (currentHealth < 0)
            {
                currentHealth = 0;
                isHelthOver = true;
                OnHealthOver(this);
            }
        }
    }

    public float Heal
    {
        set
        {
            currentHealth += value;

            if (currentHealth > _maxHealth)
                currentHealth = _maxHealth;
        }
    }

    bool isHelthOver = false;

    public HealthManagerP2D(float maxHealth,Action<object> onHealthOverFunction)
    {
        this._maxHealth = maxHealth;
        this.currentHealth = maxHealth;

        HealthOver += new HealthOverEventHandler(onHealthOverFunction);
    }

    public HealthManagerP2D(float maxHealth, float currentHealth)
    {
        this._maxHealth = maxHealth;
        this.currentHealth = currentHealth;
    }

    public void setHealth(float maxHealth, float currentHealth)
    {
        this._maxHealth = maxHealth;
        this.currentHealth = currentHealth;
    }

    public float getCurrentHealthPercent()
    {
        return currentHealth / MaxHealth;
    }

}
