﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public delegate void OnPanelEnd();

public class P2DPanel : MonoBehaviour
{

    public event OnPanelEnd OnPanelEndEvent;

    void PanelEnd()
    {
        if (OnPanelEndEvent != null)
        {

            OnPanelEndEvent();
        }
    }

    bool isShow = false;
    public bool IsShow
    {
        get { return isShow; }
    }
    public float DeactiveTime = 1;
    float showTime = 0.5f;
    float elepsedTime = 0;
    float alfa = 0;

    CanvasGroup canvasGroup;
    Animator panelAnimator;

    Coroutine hidePanelRoutine;

    public bool shouldActive = false;

    // Use this for initialization
    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        panelAnimator = GetComponent<Animator>();

        hidePanelRoutine = StartCoroutine(DeActive(DeactiveTime));

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (isShow && elepsedTime <= showTime)
        {
            elepsedTime += Time.unscaledTime;

            alfa = (elepsedTime / showTime);

            if (canvasGroup != null)
            {
                canvasGroup.alpha = alfa;
            }
        }
    }

    public void Show(float fadeInTime = 0.5f, bool setOnCenter = true)
    {
       if(hidePanelRoutine != null)
        {
            StopCoroutine(hidePanelRoutine);
        }

        gameObject.SetActive(true);

        if (setOnCenter)
        {
            transform.localPosition = Vector2.zero;
        }
        isShow = true;
        showTime = fadeInTime;
       
        if (panelAnimator != null)
        {
            panelAnimator.SetTrigger("InTrigger");
        }
    }


    /// <summary>
    /// *** Invoke deactive after 0.5 secound
    /// </summary>
    public void Hide(bool immediate = false)
    {
        if (isShow)
        {
            isShow = false;
            elepsedTime = 0;

            if (panelAnimator != null)
            {
                panelAnimator.SetTrigger("OutTrigger");
            }

            PanelEnd();

            if (!shouldActive)
            {
                if (immediate)
                {
                    hidePanelRoutine = StartCoroutine(DeActive(0));
                }
                else
                {
                    hidePanelRoutine = StartCoroutine(DeActive(DeactiveTime));
                }
            }
        }
    }

    IEnumerator DeActive(float time)
    {
        yield return new WaitForSeconds(time);

        alfa = 0;
        gameObject.SetActive(false);

    }

}
