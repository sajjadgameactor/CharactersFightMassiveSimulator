﻿using UnityEngine;
using System.Collections;

public class P2DAmountShower  {

    private int showScore = 0;
    private int CurrentScore = 0;

    public int TotalScore
    {
        get { return CurrentScore; }
    }

    public int ShowScore
    {
        get { return showScore; }
    }

	// Use this for initialization
	void Start () {
	
	}


    public void QuickScoreCalc()
    {
        showScore = TotalScore;

    }

    public bool pushPoint(int amount)
    {
        CurrentScore += amount;

        return true;
    }

    /// <summary>
    /// if we have enough Point Reduces the amount of required and return True else return false
    /// </summary>
    /// <param name="amount">required amount</param>
    /// <returns></returns>
    public bool popPoint(int amount)
    {
        if (CurrentScore < amount)
        {
            return false; // we have not enough Score
        }

        CurrentScore -= amount;

        return true;
    }


    public void Update()
    {
        if (showScore == CurrentScore)
            return;
        else if (CurrentScore > showScore)
        {
            if (CurrentScore - showScore > 10000)
                showScore += 9000;
            else if (CurrentScore - showScore > 501)
                showScore += 500;
            else if (CurrentScore - showScore > 26)
                showScore += 25;
            else
                showScore++;
        }
        else
        {
            if (showScore - CurrentScore > 10000)
                showScore -= 9000;
            else if (showScore - CurrentScore > 501)
                showScore -= 500;
            else if (showScore - CurrentScore > 26)
                showScore -= 25;
            else
                showScore--;
        }
    }
}
