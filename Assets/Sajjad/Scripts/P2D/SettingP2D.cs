﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SettingP2D {

    public static string MenuScene = "MenuScene";
    public static string HeadMiniGame = "GameScene";

    public static P2DMessegeBox MessegeBox;
    //public static InAppPurchaseScript inAppPurches;

    public static int isHighScoreOnServer
    {
        get { return PlayerPrefs.GetInt("HighScoreOnServer"); }
        set
        {
            PlayerPrefs.SetInt("HighScoreOnServer", value);
        }
    }

    private static bool _mute = false;

    public static string playerName;

    public static bool Mute
    {
        get { return _mute; }
        set
        {
            _mute = value;

            if (_mute)
            {
                PlayerPrefs.SetInt("Mute", 1);
                AudioListener.volume = 0;

            }
            else
            {
                PlayerPrefs.SetInt("Mute", 0);
                AudioListener.volume = 1;

            }
        }
    }

    private static bool isSettingP2DInit = false;

    public static void initSettingP2D()
    {
        if (!isSettingP2DInit)
        {
            //InitP2DEngine();
            LockManagerP2D.InitLocks();
            initPlayedNumber();
            getSubmitedRate();

            isSettingP2DInit = true;
        }
        else
        {
            Debug.Log("SettingP2D alredy initialized");
        }
    }

    private static void InitP2DEngine()
    {
        GameObject canvasP2D;
        if((canvasP2D = GameObject.Find("CanvasP2D")) == null)
        {
            canvasP2D = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/CanvasP2D"));

        }
        GameObject.DontDestroyOnLoad(canvasP2D);
        MessegeBox = canvasP2D.transform.Find("MessegeBoxPanel").GetComponent<P2DMessegeBox>();
        //inAppPurches = canvasP2D.transform.FindChild("PanelInAppPurches").GetComponent<InAppPurchaseScript>();
    }

    private static void initPlayedNumber()
    {
        if (PlayerPrefs.HasKey(playedNumberKey))
        {
            playedNumber = PlayerPrefs.GetInt(playedNumberKey);
            PlayerPrefs.SetInt(playedNumberKey, playedNumber);
        }
        else
        {
            PlayerPrefs.SetInt(playedNumberKey, 1);
            Debug.Log("Set Played Number");
        }
    }

    static int playedNumber = 1;
    const string playedNumberKey = "PlayedNumber";

    public static int PlayedNumber
    {
        get { return playedNumber; }
        set
        {
            playedNumber = value;
            PlayerPrefs.SetInt(playedNumberKey, playedNumber);
            Debug.Log(playedNumber);

        }
    }

    static bool submitedRate = false;
    public static bool isSubmitedRate
    {
        get { return submitedRate; }
    }
    static string submitKey = "Submit";
    static void getSubmitedRate()
    {
        if (PlayerPrefs.HasKey(submitKey))
        {
            if (PlayerPrefs.GetInt(playedNumberKey) == 1)
            {
                submitedRate = true;
            }

        }

    }
    public static IEnumerator SubmitRate()
    {
        PlayerPrefs.SetInt(playedNumberKey, 1);

        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");

        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_EDIT"));
        intentObject.Call<AndroidJavaObject>("setData", uriClass.CallStatic<AndroidJavaObject>("parse", "market://details?id=com.Phoenix2D.gelevele"));
        intentObject.Call<AndroidJavaObject>("setPackage", "com.farsitel.bazaar");

        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        currentActivity.Call("startActivity", intentObject);

        yield return new WaitForSeconds(0.1f);

    }


    public static void MuteSound(bool val)
    {
        Mute = val;
    }

}
