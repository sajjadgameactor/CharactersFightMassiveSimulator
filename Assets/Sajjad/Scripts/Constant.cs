﻿using UnityEngine;
using System.Collections;

public class Constant {
    public const int groupVersion = 17; // used for balance dss
    public const string SceneMenu = "MenuScene";
    public const string SceneGame = "GameScene";


    //public static string serverURI = "http://localhost:18032/SignalR/hubs/";
    public static string serverURI = "http://37.255.249.191/SignalR/hubs/";
    public const string getBalanceDataURL = "http://127.0.0.1/balance/Groups/GetGroupItemJson/1";
    public const string getActiveVersionURL = "http://127.0.0.1/balance/Groups/GetActiveVersion/1";
    public const string sendBalanceDataURL = "http://127.0.0.1/balance/groups/AddGameData/1";

    public const string teamColorKey = "teamColorKey";
    public const string mobileNumberKey = "mobileNumberKey";

    public const string player1Key = "player1";
    public const string player2Key = "player2";

    public const string deckKey = "deck";

}
