﻿using UnityEngine;
using System.Collections;
using BestHTTP.SignalR;
using BestHTTP.SignalR.Hubs;
using System;
using UnityEngine.UI;
using BestHTTP.SignalR.Messages;

public class ConnectionManager {

    public static Text messegeText;

    public static Connection signalRConnection;
    public static Hub gameHub;

    static bool isInit = false;
    public static void InitServerConnection()
    {
        if(!isInit)
        {
            Application.runInBackground = true;
            Uri uri = new Uri(Constant.serverURI);
            gameHub = new Hub("gameHub");
            signalRConnection = new Connection(uri, gameHub);
            signalRConnection.OnConnected += SignalRConnection_OnConnected;
            signalRConnection.OnClosed += SignalRConnection_OnClosed;
            signalRConnection.OnError += SignalRConnection_OnError;

            gameHub.On("playerJoined", OnPlayerJoined);
            gameHub.On("waitingList", OnWaitingList);
            gameHub.On("startGame", OnStartGame);
            //gameHub.On("playerJoined", OnPlayerJoined);

            signalRConnection.Open();
        }
       
    }

    private static void SignalRConnection_OnError(Connection connection, string error)
    {
        string messege = "Error:" + error;
        

    }

    private static void SignalRConnection_OnClosed(Connection connection)
    {
        string messege = "قطع اتصال".faConvert();
        myLog(messege);


    }

    private static void SignalRConnection_OnConnected(Connection connection)
    {
        string messege = "در حال اتصال".faConvert();
        myLog(messege);

    }


    public static void join(string name)
    {
        gameHub.Call("join", name);

    }

    private static void OnPlayerJoined(Hub hub, MethodCallMessage msg)
    {
        myLog((string.Format("{0} joined ", msg.Arguments[0])));
    }

    private static void OnWaitingList(Hub hub, MethodCallMessage msg)
    {
        myLog("انتظار...".faConvert());
    }

    private static void OnStartGame(Hub hub, MethodCallMessage msg)
    {
        myLog("شروع بازی");
    }


    private static void myLog(string messege)
    {
        Debug.Log(messege);
        if (messegeText != null)
        {
            messegeText.text = messege;
        }
    }

}
