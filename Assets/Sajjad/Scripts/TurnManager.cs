﻿using UnityEngine;
using System.Collections;

public enum Turn
{
    Player1 = 0,
    Player2 = 1,
}



public class TurnManager
{
    public delegate void TurnChangeEventHandler(Turn currentTurn);
    public event TurnChangeEventHandler OnTurnChange;
    private void TurnChangeEvent()
    {
        //OnPushPoint?.Invoke(CurrentScore);
        if (OnTurnChange != null)
        {
            OnTurnChange(__currentTurn);
        }
    }


    /// <summary>
    /// Warning: dont change this parameter value directly ( use SetTurn method instead )
    /// </summary>
    Turn __currentTurn = Turn.Player1; ///
    public  Turn currentTurn
    {
        get { return __currentTurn; }
      
    }

    public void SetTurn(Turn turn,int turnNumber = -1)
    {
        __currentTurn = turn;
        if (turnNumber != -1)
            currentTurnNumber = turnNumber;

        TurnChangeEvent();
    }


    public int currentTurnNumber = 1;

    public Turn GoToNextTurn()
    {
        if (currentTurn == Turn.Player1)
            SetTurn(Turn.Player2);
        else
            SetTurn(Turn.Player1);

        currentTurnNumber++;

        return __currentTurn;
    }

}
